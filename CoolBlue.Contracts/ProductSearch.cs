﻿namespace CoolBlue.Controllers.Api {
    /// <summary>
    /// Search params
    /// </summary>
    public class ProductSearch {

        /// <summary>
        /// Search keyword
        /// </summary>
        public string SearchString { get; set; }

        /// <summary>
        /// Price range bottom line
        /// </summary>
        public decimal? PriceFrom { get; set; }

        /// <summary>
        /// Price range top line
        /// </summary>
        public decimal? PriceTo { get; set; }
    }
}