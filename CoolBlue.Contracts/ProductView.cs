﻿using System.Collections.Generic;

using CoolBlue.Models;

namespace CoolBlue.Controllers.Api {
    public class ProductView : Product {
        public ProductView() {
            
        }

        public List<Bundle> Bundles { get; set; }
    }
}