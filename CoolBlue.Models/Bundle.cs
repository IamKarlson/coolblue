﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoolBlue.Models {
    public class Bundle : SellingItem {
        /// <summary>
        /// Bundle identifier
        /// </summary>
        [Key]
        public override int Id { get; set; }

        /// <summary>
        /// Bundle name
        /// </summary>
        [MinLength(5)]
        [MaxLength(300)]
        public override string Name { get; set; }

        /// <summary>
        /// Products in the bundle
        /// </summary>
        [Required]
        public List<Product> Products { get; set; }
    }
}