using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CoolBlue.Models {
    public class CoolBlueContext : DbContext {
        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }

        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<Bundle> Bundles { get; set; }

        public virtual DbSet<Customer> Customers { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

        // Your context has been configured to use a 'CoolBlueContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'CoolBlue.Models.CoolBlueContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'CoolBlueContext' 
        // connection string in the application configuration file.
        public CoolBlueContext() : base("name=CoolBlueContext") {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Entity<Order>().HasMany(t => t.Products).WithMany();
            modelBuilder.Entity<Bundle>().HasMany(t => t.Products).WithMany();
        }
    }
}