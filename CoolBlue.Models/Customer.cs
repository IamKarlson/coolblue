﻿using System.ComponentModel.DataAnnotations;

namespace CoolBlue.Models {
    public class Customer {
        /// <summary>
        /// Customer identifier
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Customer name
        /// </summary>
        [MinLength(5)]
        [MaxLength(300)]
        public string Name { get; set; }

        /// <summary>
        /// Customer email
        /// </summary>
        [EmailAddress]
        public string Email { get; set; }


        /// <summary>
        /// Customer's phone number
        /// </summary>
        [Phone]
        public string Phone { get; set; }
    }
}