using System.Data.Entity.Migrations;

namespace CoolBlue.Models.Migrations {
    public partial class Init: DbMigration {
        public override void Up() {
            CreateTable("dbo.Products",
                        c => new {
                            Id = c.Int(nullable: false, identity: true),
                            Name = c.String(maxLength: 300),
                            Description = c.String(),
                            Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                            ImageUrl = c.String(),
                            Brand = c.String(maxLength: 300),
                            Category = c.Int(nullable: false),
                            Discriminator = c.String(nullable: false, maxLength: 128),
                        }).PrimaryKey(t => t.Id);

            CreateTable("dbo.Customers",
                        c => new {
                            Id = c.Int(nullable: false, identity: true),
                            Name = c.String(maxLength: 300),
                            Email = c.String(),
                        }).PrimaryKey(t => t.Id);

            CreateTable("dbo.Orders",
                        c => new {
                            Id = c.Int(nullable: false, identity: true),
                            TotalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                            Hash = c.String(),
                            Customer_Id = c.Int(),
                        }).PrimaryKey(t => t.Id).ForeignKey("dbo.Customers", t => t.Customer_Id).Index(t => t.Customer_Id);

            CreateTable("dbo.BundleProducts",
                        c => new {
                            Bundle_Id = c.Int(nullable: false),
                            Product_Id = c.Int(nullable: false),
                        }).PrimaryKey(t => new {
                              t.Bundle_Id,
                              t.Product_Id
                          }).ForeignKey("dbo.Products", t => t.Bundle_Id).ForeignKey("dbo.Products", t => t.Product_Id)
                          .Index(t => t.Bundle_Id)
                          .Index(t => t.Product_Id);

            CreateTable("dbo.OrderProducts",
                        c => new {
                            Order_Id = c.Int(nullable: false),
                            Product_Id = c.Int(nullable: false),
                        }).PrimaryKey(t => new {
                              t.Order_Id,
                              t.Product_Id
                          }).ForeignKey("dbo.Orders", t => t.Order_Id, cascadeDelete: true)
                          .ForeignKey("dbo.Products", t => t.Product_Id, cascadeDelete: true).Index(t => t.Order_Id)
                          .Index(t => t.Product_Id);
        }

        public override void Down() {
            DropForeignKey("dbo.OrderProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.OrderProducts", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.Orders", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.BundleProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.Products");
            DropIndex("dbo.OrderProducts",
                      new[] {
                          "Product_Id"
                      });
            DropIndex("dbo.OrderProducts",
                      new[] {
                          "Order_Id"
                      });
            DropIndex("dbo.BundleProducts",
                      new[] {
                          "Product_Id"
                      });
            DropIndex("dbo.BundleProducts",
                      new[] {
                          "Bundle_Id"
                      });
            DropIndex("dbo.Orders",
                      new[] {
                          "Customer_Id"
                      });
            DropTable("dbo.OrderProducts");
            DropTable("dbo.BundleProducts");
            DropTable("dbo.Orders");
            DropTable("dbo.Customers");
            DropTable("dbo.Products");
        }
    }
}