namespace CoolBlue.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Ean", c => c.String(maxLength: 13));
            CreateIndex("dbo.Products", "Ean", unique: true, name: "UQ_EanProduct");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Products", "UQ_EanProduct");
            DropColumn("dbo.Products", "Ean");
        }
    }
}
