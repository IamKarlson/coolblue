namespace CoolBlue.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SplitBundleAndProduct : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Products", newName: "Bundles");
            DropForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.Products");
            DropForeignKey("dbo.BundleProducts", "Product_Id", "dbo.Products");
            DropIndex("dbo.Bundles", "UQ_EanProduct");
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ean = c.String(maxLength: 13),
                        Brand = c.String(maxLength: 300),
                        Category = c.Int(nullable: false),
                        Name = c.String(maxLength: 300),
                        Description = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ImageUrl = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Ean, unique: true, name: "UQ_EanProduct");
            
            AddForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.Bundles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.BundleProducts", "Product_Id", "dbo.Products", "Id", cascadeDelete: true);
            DropColumn("dbo.Bundles", "Ean");
            DropColumn("dbo.Bundles", "Brand");
            DropColumn("dbo.Bundles", "Category");
            DropColumn("dbo.Bundles", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Bundles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Bundles", "Category", c => c.Int(nullable: false));
            AddColumn("dbo.Bundles", "Brand", c => c.String(maxLength: 300));
            AddColumn("dbo.Bundles", "Ean", c => c.String(maxLength: 13));
            DropForeignKey("dbo.BundleProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.Bundles");
            DropIndex("dbo.Products", "UQ_EanProduct");
            DropTable("dbo.Products");
            CreateIndex("dbo.Bundles", "Ean", unique: true, name: "UQ_EanProduct");
            AddForeignKey("dbo.BundleProducts", "Product_Id", "dbo.Products", "Id");
            AddForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.Products", "Id");
            RenameTable(name: "dbo.Bundles", newName: "Products");
        }
    }
}
