namespace CoolBlue.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrdersFix : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Orders", new[] { "Customer_Id" });
            AlterColumn("dbo.Orders", "Customer_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "Customer_Id");
            DropColumn("dbo.Orders", "Hash");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "Hash", c => c.String());
            DropIndex("dbo.Orders", new[] { "Customer_Id" });
            AlterColumn("dbo.Orders", "Customer_Id", c => c.Int());
            CreateIndex("dbo.Orders", "Customer_Id");
        }
    }
}
