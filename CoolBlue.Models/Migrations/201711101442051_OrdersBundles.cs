namespace CoolBlue.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrdersBundles : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Bundles", newName: "SellingItems");
            RenameTable(name: "dbo.OrderProducts", newName: "OrderSellingItems");
            DropForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.Bundles");
            DropForeignKey("dbo.BundleProducts", "Product_Id", "dbo.Products");
            DropIndex("dbo.Products", "UQ_EanProduct");
            RenameColumn(table: "dbo.OrderSellingItems", name: "Product_Id", newName: "SellingItem_Id");
            RenameIndex(table: "dbo.OrderSellingItems", name: "IX_Product_Id", newName: "IX_SellingItem_Id");
            AddColumn("dbo.SellingItems", "Ean", c => c.String(maxLength: 13));
            AddColumn("dbo.SellingItems", "Brand", c => c.String(maxLength: 300));
            AddColumn("dbo.SellingItems", "Category", c => c.Int());
            AddColumn("dbo.SellingItems", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.SellingItems", "Ean", unique: true, name: "UQ_EanProduct");
            AddForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.SellingItems", "Id");
            AddForeignKey("dbo.BundleProducts", "Product_Id", "dbo.SellingItems", "Id");
            DropTable("dbo.Products");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ean = c.String(maxLength: 13),
                        Brand = c.String(maxLength: 300),
                        Category = c.Int(nullable: false),
                        Name = c.String(maxLength: 300),
                        Description = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ImageUrl = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.BundleProducts", "Product_Id", "dbo.SellingItems");
            DropForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.SellingItems");
            DropIndex("dbo.SellingItems", "UQ_EanProduct");
            DropColumn("dbo.SellingItems", "Discriminator");
            DropColumn("dbo.SellingItems", "Category");
            DropColumn("dbo.SellingItems", "Brand");
            DropColumn("dbo.SellingItems", "Ean");
            RenameIndex(table: "dbo.OrderSellingItems", name: "IX_SellingItem_Id", newName: "IX_Product_Id");
            RenameColumn(table: "dbo.OrderSellingItems", name: "SellingItem_Id", newName: "Product_Id");
            CreateIndex("dbo.Products", "Ean", unique: true, name: "UQ_EanProduct");
            AddForeignKey("dbo.BundleProducts", "Product_Id", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.Bundles", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.OrderSellingItems", newName: "OrderProducts");
            RenameTable(name: "dbo.SellingItems", newName: "Bundles");
        }
    }
}
