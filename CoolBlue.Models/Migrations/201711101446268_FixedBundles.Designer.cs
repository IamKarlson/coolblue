// <auto-generated />
namespace CoolBlue.Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class FixedBundles : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(FixedBundles));
        
        string IMigrationMetadata.Id
        {
            get { return "201711101446268_FixedBundles"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
