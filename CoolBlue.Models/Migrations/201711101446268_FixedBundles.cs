namespace CoolBlue.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedBundles : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.SellingItems", newName: "Bundles");
            RenameTable(name: "dbo.OrderSellingItems", newName: "OrderProducts");
            DropForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.SellingItems");
            DropForeignKey("dbo.BundleProducts", "Product_Id", "dbo.SellingItems");
            DropIndex("dbo.Bundles", "UQ_EanProduct");
            RenameColumn(table: "dbo.OrderProducts", name: "SellingItem_Id", newName: "Product_Id");
            RenameIndex(table: "dbo.OrderProducts", name: "IX_SellingItem_Id", newName: "IX_Product_Id");
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ean = c.String(maxLength: 13),
                        Brand = c.String(maxLength: 300),
                        Category = c.Int(nullable: false),
                        Name = c.String(maxLength: 300),
                        Description = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ImageUrl = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Ean, unique: true, name: "UQ_EanProduct");
            
            AddColumn("dbo.Bundles", "Order_Id", c => c.Int());
            CreateIndex("dbo.Bundles", "Order_Id");
            AddForeignKey("dbo.Bundles", "Order_Id", "dbo.Orders", "Id");
            AddForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.Bundles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.BundleProducts", "Product_Id", "dbo.Products", "Id", cascadeDelete: true);
            DropColumn("dbo.Bundles", "Ean");
            DropColumn("dbo.Bundles", "Brand");
            DropColumn("dbo.Bundles", "Category");
            DropColumn("dbo.Bundles", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Bundles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Bundles", "Category", c => c.Int());
            AddColumn("dbo.Bundles", "Brand", c => c.String(maxLength: 300));
            AddColumn("dbo.Bundles", "Ean", c => c.String(maxLength: 13));
            DropForeignKey("dbo.BundleProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.Bundles");
            DropForeignKey("dbo.Bundles", "Order_Id", "dbo.Orders");
            DropIndex("dbo.Products", "UQ_EanProduct");
            DropIndex("dbo.Bundles", new[] { "Order_Id" });
            DropColumn("dbo.Bundles", "Order_Id");
            DropTable("dbo.Products");
            RenameIndex(table: "dbo.OrderProducts", name: "IX_Product_Id", newName: "IX_SellingItem_Id");
            RenameColumn(table: "dbo.OrderProducts", name: "Product_Id", newName: "SellingItem_Id");
            CreateIndex("dbo.Bundles", "Ean", unique: true, name: "UQ_EanProduct");
            AddForeignKey("dbo.BundleProducts", "Product_Id", "dbo.SellingItems", "Id");
            AddForeignKey("dbo.BundleProducts", "Bundle_Id", "dbo.SellingItems", "Id");
            RenameTable(name: "dbo.OrderProducts", newName: "OrderSellingItems");
            RenameTable(name: "dbo.Bundles", newName: "SellingItems");
        }
    }
}
