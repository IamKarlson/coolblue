// <auto-generated />
namespace CoolBlue.Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class CustomerPhone : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CustomerPhone));
        
        string IMigrationMetadata.Id
        {
            get { return "201711101454069_CustomerPhone"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
