using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Text;

namespace CoolBlue.Models.Migrations {
    internal sealed class Configuration: DbMigrationsConfiguration<CoolBlueContext> {
        public Configuration() {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CoolBlueContext context) {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            var productMotorola = new Product() {
                Name = "Motorola Moto G5 Grijs",
                Category = Categories.Phone,
                Price = 179,
                Brand = "Motorola",
                ImageUrl = "https://image.coolblue.io/products/700137?height=390&width=422",
                Ean = "1000000000009",
                Description =
                    "De Motorola Moto G5 verbergt al jouw persoonlijke gegevens achter de top van je vinger. In tegenstelling tot zijn voorganger, de Moto G4, heeft de Moto G5 een vingerafdrukscanner. Verder beschikt het toestel over een 5 inch Full HD scherm. Hierop kijk je al jouw foto's terug. Die haarscherpe foto's maak je met de 13 megapixel camera. Ook is de G5 voorzien van een duurzaam jasje. De behuizing is namelijk gemaakt van sterk aluminium."
            };
            var productCover = new Product() {
                Name = "Azuri TPU Ultra Thin Motorola Moto G5 Back Cover Transparant",
                Category = Categories.Accesory,
                Price = (decimal) 16.99,
                Ean= "1000000000016",
                Description =
                    "De TPU Ultra Thin case van Azuri beschermt de Motorola Moto G5 op een stijlvolle manier, zonder dat het toestel haar uiterlijk verliest. Dankzij de vervaardiging uit hoogwaardig kunststof kan het toestel wel tegen een stootje. Ook handige uitsparingen zijn aangebracht. Voor de camera bijvoorbeeld of voor de aansluiting van je hoofdtelefoon."
            };
            context.Products.AddOrUpdate(t => t.Name,
                                         new Product() {
                                             Name = "Samsung Galaxy A5 (2017) Zwart",
                                             Brand = "Samsung",
                                             Category = Categories.Phone,
                                             Price = 349,
                                             Ean= "1000000000023",
                                             ImageUrl = "https://image.coolblue.io/products/656931?height=390&width=422",
                                             Description =
                                                 "De Samsung Galaxy A5 is nu voor het eerst waterdicht, waardoor zorgen over vocht en regen verleden tijd zijn. Ook beschikt de nieuwe Galaxy A5 over het always on display. Hiermee heb je toegang tot je belangrijkste apps zonder je scherm te ontgrendelen. Het toestel beschikt daarnaast over een 16 megapixel camera �n een 16 megapixel selfie camera, zodat al je foto's haarscherp zijn. Voor al die foto's is er ruimte genoeg, want het geheugen van de telefoon is tot 256 GB uit te breiden."
                                         },
                                         new Product() {
                                             Name = "Apple iPhone 8 64GB Space Grey",
                                             Brand = "Apple",
                                             Category = Categories.Phone,
                                             Price = 809,
                                             Ean= "1000000000030",
                                             ImageUrl = "https://image.coolblue.io/products/911335?height=390&width=422",
                                             Description =
                                                 "iPhone 8 is heeft een volledig glazen behuizing. Dit levert niet alleen een strak design op, maar maakt draadloos opladen ook mogelijk. Hierdoor heb je geen adapter meer nodig om tegelijkertijd je favoriete muziek te beluisteren en je iPhone op te laden. Met de 12 megapixel camera maak je naast scherpe foto's ook heldere video's dankzij de 4K resolutie. Maak je graag selfies? Ook in het donker cre�er je duidelijke zelfportretten met de 7 megapixel selfiecamera met flitser. Bekijk je foto's en video's op het 4,7 inch Retina HD beeldscherm met True Tone technologie. Deze technologie corrigeert de kleuren op het beeldscherm, waardoor alle kleuren er hetzelfde uitzien bij elk omgevingslicht."
                                         },
                                         productMotorola,
                                         productCover);

            context.Bundles.AddOrUpdate(t => t.Name,
                                        new Bundle() {
                                            Name = $"{productMotorola.Name} + {productCover.Name}",
                                            Price = (productMotorola.Price + productCover.Price) * (decimal) 0.95,
                                            Products = new List<Product>() {
                                                productMotorola,
                                                productCover
                                            }
                                        });
            SaveChanges(context);
        }

        private void SaveChanges(DbContext context) {
            try { context.SaveChanges(); }
            catch(DbEntityValidationException ex) {
                StringBuilder sb = new StringBuilder();

                foreach(var failure in ex.EntityValidationErrors) {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach(var error in failure.ValidationErrors) {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException("Entity Validation Failed - errors follow:\n" + sb.ToString(),
                                                      ex); // Add the original exception as the innerException
            }
        }
    }
}