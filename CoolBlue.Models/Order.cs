﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoolBlue.Models {
    public class Order {
        /// <summary>
        /// Order identifier
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Customer which made the order
        /// </summary>
        [Required]
        public Customer Customer { get; set; }

        /// <summary>
        /// Products or bundles in the order
        /// </summary>
        public List<Product> Products { get; set; }

        /// <summary>
        /// Bought bundles
        /// </summary>
        public List<Bundle> Bundles { get; set; }


        /// <summary>
        /// Total order price
        /// </summary>
        public decimal TotalPrice { get; set; }
    }
}