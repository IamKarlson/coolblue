﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoolBlue.Models {
    public class Product : SellingItem {
        /// <summary>
        /// European article number
        /// </summary>
        [Index("UQ_EanProduct", IsUnique = true)]
        [MinLength(13)]
        [MaxLength(13)]
        public string Ean { get; set; }

        [MinLength(2)]
        [MaxLength(300)]
        public string Brand { get; set; }

        /// <summary>
        /// Product category
        /// </summary>
        public Categories Category { get; set; }
    }
}