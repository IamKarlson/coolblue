﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CoolBlue.Models.Repositories {
    public class BundlesRepository : IBundlesRepository {
        private readonly CoolBlueContext db;
        /// <summary>
        /// ctor
        /// </summary>
        public BundlesRepository() {
            db = new CoolBlueContext();
        }

        /// <inheritdoc />
        public async Task<List<Bundle>> GetBundlesForProductAsync(int productId) {
            return await db.Bundles.Where(t => t.Products.Any(p => p.Id == productId)).ToListAsync();
        }
    }
}