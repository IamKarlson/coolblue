﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoolBlue.Models.Repositories {
    public interface IBundlesRepository {
        Task<List<Bundle>> GetBundlesForProductAsync(int productId);
    }
}