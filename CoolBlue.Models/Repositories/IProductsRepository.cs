﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoolBlue.Models.Repositories {
    public interface IProductsRepository {
        /// <summary>
        /// Return all the products
        /// </summary>
        /// <param name="skip">Pagging skip</param>
        /// <param name="limit">Pagging limit</param>
        /// <returns>List of products</returns>
        Task<List<Product>> Products(uint? skip = null, uint? limit = null);

        /// <summary>
        /// Get product by database ID
        /// </summary>
        /// <param name="id">Id to search</param>
        /// <returns>Product</returns>
        Task<Product> GetProduct(int id);

        /// <summary>
        /// Look up for a product using its name or description case insensitive
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        Task<List<Product>> SearchProduct(string keyword);

        /// <summary>
        /// Get product by EAN
        /// </summary>
        /// <param name="ean">EAN to search</param>
        /// <returns>Product</returns>
        Task<Product> GetProduct(string ean);

        /// <summary>
        /// Return all the products matching price range
        /// </summary>
        /// <param name="minPrice">Minumum allowed price</param>
        /// <param name="maxPrice">Maximum allowed price</param>
        /// <returns>List of products</returns>
        Task<List<Product>> GetProductByPrice(decimal minPrice, decimal maxPrice);
    }
}