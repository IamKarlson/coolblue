﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CoolBlue.Models.Repositories {
    /// <inheritdoc />
    public class ProductsRepository : IProductsRepository {
        private readonly CoolBlueContext db;

        /// <summary>
        /// ctor
        /// </summary>
        public ProductsRepository() {
            db = new CoolBlueContext();
        }

        /// <inheritdoc />
        public async Task<List<Product>> Products(uint? skip = null, uint? limit = null) {
            IQueryable<Product> products = db.Products;
            if (skip != null) { products = products.OrderBy(t=>t.Id).Skip((int)skip); }
            if (limit != null) { products = products.OrderBy(t=>t.Id).Take((int)limit); }
            return await products.ToListAsync();
        }

        /// <inheritdoc />
        public async Task<Product> GetProduct(int id) {
            return await db.Products.FindAsync(id);
        }

        /// <inheritdoc />
        public async Task<Product> GetProduct(string ean) {
            return await db.Products.Where(t => t.Ean == ean).SingleOrDefaultAsync();
        }

        /// <inheritdoc />
        public async Task<List<Product>> GetProductByPrice(decimal minPrice, decimal maxPrice) {
            return await db.Products.Where(t => minPrice <= t.Price && t.Price <= maxPrice).ToListAsync();
        }

        /// <inheritdoc />
        public async Task<List<Product>> SearchProduct(string keyword) {
            return await db.Products
                .Where(t =>
                    t.Name.Contains(keyword)
                    || t.Brand.Contains(keyword)
                    || t.Description.Contains(keyword))
                .ToListAsync();
        }
    }
}