﻿using System.ComponentModel.DataAnnotations;

namespace CoolBlue.Models {
    public abstract class SellingItem {
        /// <summary>
        /// Product identifier
        /// </summary>
        [Key]
        public virtual int Id { get; set; }

        /// <summary>
        /// Product name
        /// </summary>
        [MinLength(5)]
        [MaxLength(300)]
        public virtual string Name { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// Product price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Product image
        /// </summary>
        [Url]
        public string ImageUrl { get; set; }
    }
}