﻿using System.Collections.Generic;
using System.Threading.Tasks;

using CoolBlue.Models;

namespace CoolBlue.Controllers.Api {
    /// <summary>
    /// Manipulating of products from the storage
    /// </summary>
    public interface IProductsService {
        /// <summary>
        /// Get product list with paggin
        /// </summary>
        /// <param name="skip">Pagging skip</param>
        /// <param name="limit">Pagging limit</param>
        /// <returns>List of products</returns>
        Task<List<ProductView>> GetProductsAsync(uint? skip, uint? limit);

        /// <summary>
        /// Searching products in the storage
        /// </summary>
        /// <param name="dto">Search configuration</param>
        /// <returns>List of products which matching criteria</returns>
        Task<List<ProductView>> SearchProduct(ProductSearch dto);

        /// <summary>
        /// Get one particular product
        /// </summary>
        /// <param name="id">Product unique integer identifier</param>
        /// <param name="includeBundles">Include bundles for this product or not</param>
        /// <returns>Found product otherwise null</returns>
        Task<ProductView> GetProductAsync(int id, bool includeBundles = false);

        /// <summary>
        /// Get one particular product
        /// </summary>
        /// <param name="Ean">Product EAN</param>
        /// <returns>Found product otherwise null</returns>
        Task<ProductView> GetProductAsync(string Ean);
    }
}