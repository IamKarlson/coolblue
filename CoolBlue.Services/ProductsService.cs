﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ClassLibrary1;

using CoolBlue.Contracts;
using CoolBlue.Models;
using CoolBlue.Models.Repositories;

namespace CoolBlue.Controllers.Api {
    public class ProductsService : IProductsService {
        private readonly IProductsRepository productRep;
        private IBundlesRepository bundlesRepository;

        public ProductsService(IProductsRepository productRep, IBundlesRepository bundlesRepository) {
            this.productRep = productRep;
            this.bundlesRepository = bundlesRepository;
        }

        /// <inheritdoc />
        public async Task<List<ProductView>> GetProductsAsync(uint? skip, uint? limit) {
            List<Product> products = await productRep.Products(skip, limit);
            return products.Select(t => t.ToView()).ToList();
        }

        /// <inheritdoc />
        public async Task<List<ProductView>> SearchProduct(ProductSearch dto) {
            List<Product> products;
            if (string.IsNullOrWhiteSpace(dto.SearchString)) {
                products = await productRep.GetProductByPrice(dto.PriceFrom ?? 0, dto.PriceTo ?? Decimal.MaxValue);
            } else {
                products = await productRep.SearchProduct(dto.SearchString);
                if (dto.PriceFrom.HasValue) { products = products.Where(t => dto.PriceFrom <= t.Price).ToList(); }
                if (dto.PriceTo.HasValue) { products = products.Where(t => t.Price <= dto.PriceTo).ToList(); }
            }
            return products.Select(t => t.ToView()).ToList();
        }

        public async Task<ProductView> GetProductAsync(string ean) {
            if (!EanUtils.IsValidGtin(ean)) { throw new ArgumentException($"EAN is not valid"); }
            if (ean.Length != 13) { throw new ArgumentOutOfRangeException(nameof(ean), "Non EAN code was passed"); }
            Product productByEan = await productRep.GetProduct(ean);
            if (productByEan == null) { throw new ArgumentOutOfRangeException(nameof(ean), "Product with that EAN hasn't been found"); }
            return productByEan.ToView();
        }

        public async Task<ProductView> GetProductAsync(int id, bool includeBundles = false) {
            var product = await productRep.GetProduct(id);
            if (product == default(Product)) {
                throw new ArgumentOutOfRangeException(nameof(id), "Product with such ID not found");
            }
            if (!includeBundles) { return product.ToView(); }
            var bundles = await this.bundlesRepository.GetBundlesForProductAsync(id);
            return product.ToView(bundles);
        }
    }
}