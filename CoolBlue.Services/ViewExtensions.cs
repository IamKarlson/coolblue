﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CoolBlue.Controllers.Api;
using CoolBlue.Models;

using Nelibur.ObjectMapper;

namespace CoolBlue.Contracts {
    public static class ViewExtensions {
        public static ProductView ToView(this Product product, List<Bundle> bundles=null) {
            var view = TinyMapper.Map<ProductView>(product);
            if(bundles != null) {
                view.Bundles = bundles;
            }
            return view;
        }
    }
}
