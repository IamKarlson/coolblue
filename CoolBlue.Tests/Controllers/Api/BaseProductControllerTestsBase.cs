using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using CoolBlue.Models;
using CoolBlue.Models.Repositories;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

namespace CoolBlue.Controllers.Api.Tests {
    public abstract class BaseProductControllerTestsBase {
        protected Product product1;
        protected Product product2;
        protected IProductsService productService;

        [TestInitialize]
        public void Initialize() {
            product1 = new Product() {
                Id = 1,
                Name = "phone",
                Ean = "4006381333931"
            };
            product2 = new Product() {
                Id = 2,
                Name = "laptop",
                Ean = "9780471117094"
            };
            Mock<IProductsRepository> productRepMock = getProductRepMock();
            Mock<IBundlesRepository> bundlesRepMock = getBundlesRepositoryMock();
            productService = new ProductsService(productRepMock.Object, bundlesRepMock.Object);
        }

        private Mock<IBundlesRepository> getBundlesRepositoryMock() {
            var bundlesMock = new Mock<IBundlesRepository>();
            bundlesMock.Setup(t => t.GetBundlesForProductAsync(product1.Id)).ReturnsAsync(new List<Bundle>() {
                new Bundle(){Products = new List<Product>(){product1}},
            });
            return bundlesMock;

        }

        private Mock<IProductsRepository> getProductRepMock() {
            var productRepMock = new Mock<IProductsRepository>();
            productRepMock.Setup(t => t.GetProduct(0)).Returns((int id) => Task.FromResult(default(Product)));
            productRepMock.Setup(t => t.GetProduct(It.IsInRange(1, int.MaxValue, Range.Inclusive))).Returns(
                (int id) => Task.FromResult(new Product() {
                    Id = id
                }));
            var products = new List<Product>() {
                product1,
                product2
            };
            productRepMock.Setup(t => t.Products(null, null)).Returns((uint? skip, uint? limit) => Task.FromResult(products));
            productRepMock.Setup(t => t.Products(It.IsAny<uint>(), It.IsAny<uint>())).Returns((uint? skip, uint? limit) => {
                List<Product> result = products.Skip((int)(skip ?? 0)).Take((int)(limit ?? 0)).ToList();
                return Task.FromResult(result);
            });
            productRepMock.Setup(t => t.GetProduct(It.IsAny<string>())).Returns((string dto) => {
                if (Regex.IsMatch(dto, "\\d{13}")) {
                    if (product1.Ean == dto) { return Task.FromResult(product1); }
                    if (product2.Ean == dto) { return Task.FromResult(product2); }
                }
                throw new ArgumentOutOfRangeException();
            });

            productRepMock.Setup(t => t.SearchProduct(It.IsAny<string>())).Returns((string keyword) => {
                var list = products.Where(t => (t.Name?.ToLower().Contains(keyword.ToLower()) ?? false)
                                               || (t.Brand?.ToLower().Contains(keyword.ToLower()) ?? false)
                                               || (t.Description?.ToLower().Contains(keyword.ToLower()) ?? false)).ToList();
                return Task.FromResult(list);
            });
            return productRepMock;
        }
    }
}