﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Results;

using CoolBlue.Models;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoolBlue.Controllers.Api.Tests {
    [TestClass()]
    public class ProductsControllerTests: BaseProductControllerTestsBase {
        [TestMethod()]
        public async Task GetProductsNotFoundAsyncTest() {
            var ct = new ProductsController(productService);
            var actionResult = await ct.GetProductAsync(0);

            // if your action returns: NotFound()
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod()]
        public async Task GetProductsAsyncTest() {
            var ct = new ProductsController(productService);
            var actionResult = await ct.GetProductAsync(5);

            // if your action returns: NotFound()
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<ProductView>));
            var content = (actionResult as OkNegotiatedContentResult<ProductView>).Content;

            Assert.AreEqual(5, content.Id);
        }

        [TestMethod()]
        public async Task GetProductFilteredTest() {
            var ct = new ProductsController(productService);

            var action1Result = await ct.GetProductsAsync(skip: 0, limit: 1);
            Assert.IsInstanceOfType(action1Result, typeof(OkNegotiatedContentResult<List<ProductView>>));
            var content1 = (action1Result as OkNegotiatedContentResult<List<ProductView>>).Content;
            Assert.AreEqual(1, content1.Count);

            var action2Result = await ct.GetProductsAsync(skip: 1, limit: 1);
            Assert.IsInstanceOfType(action2Result, typeof(OkNegotiatedContentResult<List<ProductView>>));
            var content2 = (action2Result as OkNegotiatedContentResult<List<ProductView>>).Content;
            Assert.AreEqual(1, content2.Count);

            Assert.AreNotEqual(content1[0].Id, content2[0].Id);
        }

        [TestMethod()]
        public async Task GetProductTest() {
            var ct = new ProductsController(productService);
            var actionResult = await ct.GetProductsAsync();
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<ProductView>>));

            var content = (actionResult as OkNegotiatedContentResult<List<ProductView>>).Content;
            Assert.AreEqual(2, content.Count);
        }
        
        [TestMethod()]
        public async Task GetProductWithBundlesAsyncTest() {
            var ct = new ProductsController(productService);
            var actionResult = await ct.GetProductAsync(product1.Id,true);

            // if your action returns: NotFound()
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<ProductView>));
            var content = (actionResult as OkNegotiatedContentResult<ProductView>).Content;

            Assert.AreEqual(product1.Id, content.Id);

            Assert.AreEqual(1,content.Bundles.Count);
        }


    }
}