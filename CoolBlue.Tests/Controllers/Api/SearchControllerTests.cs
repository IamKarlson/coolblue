﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Results;

using CoolBlue.Models;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoolBlue.Controllers.Api.Tests {
    [TestClass()]
    public class SearchProductControllerTests: BaseProductControllerTestsBase {
        [TestMethod()]
        public async Task GetProductsNotFoundAsyncTest() {
            var ct = new SearchProductController(productService);
            var actionResult = await ct.GetByEan("1234567890");

            // if your action returns: NotFound()
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod()]
        public async Task GetProductsAsyncTest() {
            var ct = new SearchProductController(productService);
            var actionResult = await ct.GetByEan(product1.Ean);

            // if your action returns: NotFound()
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<ProductView>));
            var content = (actionResult as OkNegotiatedContentResult<ProductView>).Content;

            Assert.AreEqual(product1.Name, content.Name);
        }

        [TestMethod]
        public async Task ComplexSearchTest() {
            var ct = new SearchProductController(productService);
            var actionResult = await ct.ComplexSearch(new ProductSearch() {
                SearchString = product1.Name.Substring(2, 3)
            });

            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<ProductView>>));
            var content = (actionResult as OkNegotiatedContentResult<List<ProductView>>).Content;

            var result = content.Any(t => t.Id == product1.Id);
            Assert.IsTrue(result);
        }
    }
}