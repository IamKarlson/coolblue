﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using CoolBlue.Models;

using Swashbuckle.Swagger.Annotations;

namespace CoolBlue.Controllers.Api {
    /// <summary>
    /// Product operations. Available get all the products or one particular.
    /// </summary>
    [RoutePrefix("api/Products")]
    public class ProductsController : ApiController {
        private readonly IProductsService productsService;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="productsService"></param>
        public ProductsController(IProductsService productsService) {
            this.productsService = productsService;
        }

        /// <summary>
        /// Return the whole list of the availeble products
        /// GET: api/Products
        /// </summary>
        /// <param name="skip">Pagging params for skipping</param>
        /// <param name="limit">Pagging params for page size</param>
        /// <returns>Product list with pagging sorted by creation date</returns>
        [SwaggerResponse(HttpStatusCode.OK, "Products list", typeof(IEnumerable<ProductView>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Products list is empty")]
        [HttpGet]
        public async Task<IHttpActionResult> GetProductsAsync(uint? skip = null, uint? limit = null) {
            var products = await productsService.GetProductsAsync(skip, limit);
            if (!(products?.Any() ?? false)) { return NotFound(); }
            return Ok(products);
        }

        /// <summary>
        /// Get product by id.
        /// GET: api/Products/5
        /// </summary>
        /// <param name="id">Product id</param>
        /// <param name="includeBundles">You can optional include bundles</param>
        /// <returns>Product view</returns>
        [ResponseType(typeof(Product))]
        [SwaggerResponse(HttpStatusCode.OK, "Found product by id", typeof(ProductView))]
        [SwaggerResponse(HttpStatusCode.NotFound, "There's not such product with present id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetProductAsync(int id, bool includeBundles = false) {
            try {
                var product = await productsService.GetProductAsync(id, includeBundles);
                if (product == null) { return NotFound(); }

                return Ok(product);
            } catch (ArgumentException ex) { return NotFound(); }
        }
    }
}