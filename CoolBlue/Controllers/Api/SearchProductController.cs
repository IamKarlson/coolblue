﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

using Swashbuckle.Swagger.Annotations;

namespace CoolBlue.Controllers.Api {
    [RoutePrefix("api/SearchProducts")]
    public class SearchProductController : ApiController {
        private readonly IProductsService productsService;

        public SearchProductController(IProductsService productsService) {
            this.productsService = productsService;
        }

        /// <summary>
        /// Search product by EAN
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [SwaggerResponse(HttpStatusCode.OK, "Found product by EAN", typeof(ProductView))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Found product by EAN")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByEan(string ean) {
            try {
                var product = await productsService.GetProductAsync(ean);
                return Ok(product);
            } catch (ArgumentOutOfRangeException ex) { return NotFound(); } catch (ArgumentException ex) { return NotFound(); }
        }

        /// <summary>
        /// Search product by passed params
        /// POST: api/SearchProduct
        /// </summary>
        /// <param name="searchCriteria">Complex search object. Search keyword (case insensetive) for lookup in Name or Description</param>
        [SwaggerResponse(HttpStatusCode.OK, "Found product list", typeof(List<ProductView>))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Nothing found by specified criteria")]
        [HttpPost]
        public async Task<IHttpActionResult> ComplexSearch([FromBody] ProductSearch searchCriteria) {

            if(searchCriteria == null) {
                ModelState.AddModelError(nameof(searchCriteria),new ArgumentNullException(nameof(searchCriteria),"Passed were not deserialized, possibly you made a mistake in the schema"));
                return BadRequest(ModelState);
            }
            try {
                var product = await productsService.SearchProduct(searchCriteria);
                return Ok(product);
            } catch (ArgumentOutOfRangeException ex) { return NotFound(); } catch (ArgumentException ex) { return NotFound(); }
        }
    }
}